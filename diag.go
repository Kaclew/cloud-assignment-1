package CloudTask

import (
	"encoding/json"
	"net/http"
	"time"
)

//Struct that gives the data about the status of servers and about the program
type diag struct {
	GBIF          string `json:"gbif"`
	RESTCountries string `json:"restcountries"`
	Version       string `json:"version"`
	Uptime        string `json:"uptime"`
}

//Time since start
var t0 time.Time

//Finds out if there is a connection to restcountries site
func getCountry(status diag) diag {
	res, err := http.Get("https://restcountries.eu")
	if err != nil {
		status.RESTCountries = "503"
	} else {
		status.RESTCountries = "200"
	}
	res.Body.Close()

	return status
}

//Finds out if there is a connection to gbif site
func getGBIF(status diag) diag {

	res, err := http.Get("http://api.gbif.org")
	if err != nil {
		status.GBIF = "503"
	} else {
		status.GBIF = "200"
	}
	res.Body.Close()

	return status
}

//GiveTime gets the time since the program started.
func GiveTime(t time.Time) {
	t0 = t
}

//DiagHandler gets and shows all data about the servers and itself.
func DiagHandler(wri http.ResponseWriter, req *http.Request) {
	http.Header.Add(wri.Header(), "Content-type", "application/json")

	var status diag

	status = getCountry(status)
	status = getGBIF(status)
	status.Version = "v1"
	status.Uptime = time.Since(t0).String()

	json.NewEncoder(wri).Encode(status)
}
