package CloudTask

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

var gbifSpeciesPage = "http://api.gbif.org/v1/species/"

//species struct contains all info about species
type species struct {
	Key       int    `json:"speciesKey"`
	Kindgom   string `json:"kingdom"`
	Phylum    string `json:"phylum"`
	Order     string `json:"order"`
	Family    string `json:"family"`
	Genus     string `json:"genus"`
	ScienName string `json:"scientificName"`
	CanonName string `json:"canonicalName"`
	Year      string `json:"year"`
}

//Contains the year which will be added to the spcies struct
type yearStruct struct {
	Year string `json:"year"`
}

//getSpecies gets information about the parsed species name excluding year.
func getSpecies(id string) species {
	var singleSpecies species
	res, err := http.Get(gbifSpeciesPage + id)
	if err != nil {
		fmt.Println("Couldn't receive species.")
	} else {
		data, _ := ioutil.ReadAll(res.Body)
		err = json.Unmarshal(data, &singleSpecies)
	}

	res.Body.Close()

	return singleSpecies
}

//getYear gets only year for parsed species.
func getYear(id string) yearStruct {
	var singleYear yearStruct

	res, err := http.Get(gbifSpeciesPage + id + "/name")
	if err != nil {
		fmt.Println("Couldn't receive year.")
	} else {
		data, _ := ioutil.ReadAll(res.Body)
		err = json.Unmarshal(data, &singleYear)
	}

	res.Body.Close()

	return singleYear
}

//SpeciesHandler handles all connections to species
func SpeciesHandler(wri http.ResponseWriter, req *http.Request) {
	http.Header.Add(wri.Header(), "Content-type", "application/json")

	var species species
	var year yearStruct

	parts := strings.Split(req.URL.Path, "/")

	fmt.Println(parts)
	if parts[4] != "" {
		species = getSpecies(parts[4])
		year = getYear(parts[4])
		species.Year = year.Year
		json.NewEncoder(wri).Encode(species)
	} else {
		http.Error(wri, "Error 404. \nPlease write code of the species", http.StatusNotFound)
	}
}
