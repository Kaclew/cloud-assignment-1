package CloudTask

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

var countriesPage = "https://restcountries.eu/rest/v2/alpha/"
var gbifOccurrencePage = "http://api.gbif.org/v1/occurrence/search/"

// Country is a template for all countries from RESTcountries
type Country struct {
	Code string `json:"alpha2code"`
	Name string `json:"name"`
	Flag string `json:"flag"`
}

//Occurrence gets all arrays with the neccessary species information
type Occurrence struct {
	Result []Results `json:"results"`
}

//Results is a struct with the info itself
type Results struct {
	Species    string `json:"species"`
	SpeciesKey int    `json:"speciesKey"`
}

//CombinedStruct is the struct with all necessary data
type CombinedStruct struct {
	Code       string   `json:"alpha2code"`
	Name       string   `json:"name"`
	Flag       string   `json:"flag"`
	Species    []string `json:"species"`
	SpeciesKey []int    `json:"speciesKey"`
}

//CountryGet Gets the countrries from RESTcountries site and returns
//struct with all the information
func countryGet(country string) Country {
	var singleCountry Country

	res, err := http.Get(countriesPage + country)
	if err != nil {
		fmt.Println("Couldn't receive country")
	} else {
		data, _ := ioutil.ReadAll(res.Body)
		err = json.Unmarshal(data, &singleCountry)
	}

	res.Body.Close()

	return singleCountry
}

//SpeciesGet gets all species for a country.
func speciesGet(country string) Occurrence {
	var singleCountrySpecies Occurrence

	res, err := http.Get(gbifOccurrencePage + "?country=" + country)
	if err != nil {
		fmt.Println("Couldn't receive species.")
	} else {
		data, _ := ioutil.ReadAll(res.Body)
		err = json.Unmarshal(data, &singleCountrySpecies)
	}

	res.Body.Close()

	return singleCountrySpecies
}

//SpeciesGetLimit gets all species for a country with the respect to limit.
func speciesGetLimit(country string, limit string) Occurrence {
	var singleCountrySpecies Occurrence
	res, err := http.Get(gbifOccurrencePage + "?country=" + country + "&limit=" + limit)
	if err != nil {
		fmt.Println("Couldn't receive species.")
	} else {
		data, _ := ioutil.ReadAll(res.Body)
		err = json.Unmarshal(data, &singleCountrySpecies)
	}

	res.Body.Close()

	return singleCountrySpecies
}

//CountryHandler shows all the countries and its info
func CountryHandler(wri http.ResponseWriter, req *http.Request) {
	var countries Country
	var species Occurrence
	var data CombinedStruct

	http.Header.Add(wri.Header(), "Content-type", "application/json")

	parts := strings.Split(req.URL.Path, "/")

	limit := req.FormValue("limit")

	if parts[4] != "" && limit != "" {
		countries = countryGet(parts[4])
		species = speciesGetLimit(parts[4], limit)
		species = removeDupilcates(species)
		data = combine(countries, species)
		json.NewEncoder(wri).Encode(data)
	} else if parts[4] != "" {
		countries = countryGet(parts[4])
		species = speciesGet(parts[4])
		species = removeDupilcates(species)
		data = combine(countries, species)
		json.NewEncoder(wri).Encode(data)
	} else {
		http.Error(wri, "Error 404. Please write alphacode of a country", http.StatusNotFound)
	}
}

//Combines the Occurrence struct with all the species info with
//Country struct and create a new struct
func combine(country Country, species Occurrence) CombinedStruct {
	var combined CombinedStruct

	combined.Code = country.Code
	combined.Name = country.Name
	combined.Flag = country.Flag

	for i := 0; i < len(species.Result); i++ {
		combined.Species = append(combined.Species, species.Result[i].Species)
		combined.SpeciesKey = append(combined.SpeciesKey, species.Result[i].SpeciesKey)
	}

	return combined
}

//Creates new Occurrence struct which gets new entries only
//if the parsed Occurence species name is not repeated
func removeDupilcates(species Occurrence) Occurrence {
	var removed Occurrence

	for i := 0; i < len(species.Result); i++ {
		var exists = false
		for j := 0; j < len(removed.Result); j++ {
			if species.Result[i].Species == removed.Result[j].Species {
				exists = true
			}
		}
		if exists == false {
			removed.Result = append(removed.Result, species.Result[i])
		}
	}
	return removed
}
