package main

import (
	"CloudTask"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

func handlerNil(wri http.ResponseWriter, req *http.Request) {
	http.Error(wri, "Error 404. \nPlease try using one of the following: /conservation/v1/(country/spieces/diag)", http.StatusNotFound)
}

func main() {
	T0 := time.Now()
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	CloudTask.GiveTime(T0)

	http.HandleFunc("/", handlerNil)
	http.HandleFunc("/conservation/v1/country/", CloudTask.CountryHandler)
	http.HandleFunc("/conservation/v1/species/", CloudTask.SpeciesHandler)
	http.HandleFunc("/conservation/v1/diag/", CloudTask.DiagHandler)
	fmt.Println("Listening on port " + port)

	log.Fatal(http.ListenAndServe(":"+port, nil))
}
